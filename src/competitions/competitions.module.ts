import { NestjsQueryGraphQLModule, PagingStrategies } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import { Module } from '@nestjs/common';
import { UpdateCategoryInput } from 'src/category/dto/update-category.input';
import { CreateCompetitionInput } from './dto/create-competition.input';
import { Competition } from './entities/competition.entity';

@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([Competition])],
      resolvers: [
        {
          EntityClass: Competition,
          DTOClass: Competition,
          CreateDTOClass: CreateCompetitionInput,
          UpdateDTOClass: UpdateCategoryInput,
          enableAggregate: true,
          pagingStrategy: PagingStrategies.NONE,
        }
      ]
    })
  ],
  providers: []
})
export class CompetitionsModule {}
