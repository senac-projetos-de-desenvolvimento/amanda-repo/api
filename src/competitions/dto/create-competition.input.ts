import { InputType, Int, Field } from '@nestjs/graphql';

@InputType('CompetitionInput')
export class CreateCompetitionInput {
  @Field()
  name: string;
  
  @Field({ nullable: true })
  season: number;

  
  @Field({ nullable: true })
  date: string;

  
  @Field({ nullable: true })
  place: string;

  
  @Field({ nullable: true })
  weight: string;
}
