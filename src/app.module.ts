import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BoxModule } from './box/box.module';
import { AthletesModule } from './athletes/athletes.module';
import { CompetitionsModule } from './competitions/competitions.module';
import { CategoryModule } from './category/category.module';
import { WorkoutTypeModule } from './workout-type/workout-type.module';
import { ResultsModule } from './results/results.module';
import { WorkoutsModule } from './workouts/workouts.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      database: 'crossrankingdb',
      username: 'postgres',
      password: 'postgres',
      autoLoadEntities: true,
      synchronize: true,
      logging: true,
    }),
    GraphQLModule.forRoot({
      // set to true to automatically generate schema
      autoSchemaFile: true,
    }),
    BoxModule,
    AthletesModule,
    CompetitionsModule,
    CategoryModule,
    WorkoutTypeModule,
    ResultsModule,
    WorkoutsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}