import { NestjsQueryGraphQLModule, PagingStrategies } from '@nestjs-query/query-graphql';
import { NestjsQueryTypeOrmModule } from '@nestjs-query/query-typeorm';
import { Module } from '@nestjs/common';
import { CreateResultInput } from './dto/create-result.input';
import { UpdateResultInput } from './dto/update-result.input';
import { Result } from './entities/result.entity';


@Module({
  imports: [
    NestjsQueryGraphQLModule.forFeature({
      imports: [NestjsQueryTypeOrmModule.forFeature([Result])],
      resolvers: [
        {
          EntityClass: Result,
          DTOClass: Result,
          CreateDTOClass: CreateResultInput,
          UpdateDTOClass: UpdateResultInput,
          enableAggregate: true,
          pagingStrategy: PagingStrategies.NONE,
        }
      ]
    })
  ],
  providers: []
})
export class ResultsModule {}
